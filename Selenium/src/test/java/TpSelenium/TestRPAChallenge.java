package TpSelenium;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestRPAChallenge {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement lastName;
	WebElement firstName; 
	WebElement	roleInCompany;
	WebElement	email;
	WebElement	companyName; 
	WebElement	phoneNumber; 
	WebElement	adresse;
	WebElement	submit;
	WebElement  start;
	
	@Test
	public void naviguer() throws IOException {
		
		/* instanciation du driver et implicit wait */
		
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver,10);
		
		/*  Se connecter à l'url   */
		
		driver.get("https://www.rpachallenge.com/");
		driver.manage().window().maximize();
		System.out.println("Je suis la0");
		
		start = driver.findElement(By.xpath("//button[contains(text(),'Start')]"));
		start.click();
		
		/* ********** Charger le JDD dans la class de test  ***********  */
		ArrayList<Map<String, String>> 	listJdd = loadCsvJDD();
		
		for (int i=0 ; i<=10 ; i++) {
		
		
		lastName = driver.findElement(By.xpath("//input[@ng-reflect-name='labelLastName']"));
		firstName = driver.findElement(By.xpath("//input[@ng-reflect-name='labelFirstName']"));
		roleInCompany = driver.findElement(By.xpath("//input[@ng-reflect-name='labelCompanyName']"));
		email = driver.findElement(By.xpath("//input[@ng-reflect-name='labelEmail']"));
		companyName = driver.findElement(By.xpath("//input[@ng-reflect-name='labelCompanyName']"));
		phoneNumber = driver.findElement(By.xpath("//input[@ng-reflect-name='labelPhone']"));
		adresse = driver.findElement(By.xpath("//input[@ng-reflect-name='labelAddress']"));
		submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		System.out.println(i);
		
		
		
		lastName.sendKeys(listJdd.get(i).get("First Name"));
		firstName.sendKeys(listJdd.get(i).get("Last Name"));
		roleInCompany.sendKeys(listJdd.get(i).get("Role in Company"));
		email.sendKeys(listJdd.get(i).get("Email"));
		companyName.sendKeys(listJdd.get(i).get("Company Name"));
		phoneNumber.sendKeys(listJdd.get(i).get("Phone Number"));
		adresse.sendKeys(listJdd.get(i).get("Address"));
		
		submit.click();
		
		}
		
			
	}
	
	public ArrayList<Map<String, String>> loadCsvJDD () throws IOException {
		String csvFilePath = "src/main/java/challenge1.csv";
		ArrayList<Map<String, String>> listJDD = new ArrayList<>();
		List<String[]> list = Files.lines(Paths.get(csvFilePath)).map(line -> line.split("\\\\r?\\\\n")).collect(Collectors.toList());
		
	    	for (int j = 1; j < list.size(); j++) {
	      	Map<String, String> jdd = new HashMap<>();
	    	String[] titres = list.get(0)[0].split(",");
	    	String[] val = list.get(j)[0].split((","));
	        	for (int i = 0; i < titres.length; i++) {
		            jdd.put(titres[i], val[i]);
		        }
		    listJDD.add(jdd);
		    }
		return listJDD;	
	}
}
