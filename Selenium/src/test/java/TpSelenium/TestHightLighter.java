package TpSelenium;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestHightLighter {
	WebDriver driver;
	WebElement signIn;
	WebElement userName;
	WebElement logIn;
	WebElement myAccount;
	WebElement menu1;
	Select select1;
	WebElement menu2;
	Select select2;
	WebElement checkbox1;
	WebElement checkbox2;
	WebElement motDePasse;
    WebDriverWait wait;
    
    
	@Test
	public void naviguer() {
		
		/* instanciation du driver et implicit wait */
		
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver,10);
		
		/*  Se connecter à l'url   */
		
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		driver.manage().window().maximize();
		
		signIn = driver.findElement(By.linkText("Sign In"));
		highLighterClick(signIn);
		
		
		
         /*     Identification      */
		
		userName = driver.findElement(By.name("username"));
		motDePasse = driver.findElement(By.name("password"));
		
		highLighterClick(userName);
		userName.clear();
		highLighterKeys(userName, "j2ee");
		
		highLighterClick(motDePasse);
		motDePasse.clear();
		highLighterKeys(motDePasse, "j2ee");
		
     	logIn = driver.findElement(By.name("signon"));
     	highLighterClick(logIn);
		
		
		/*       Se connecter à My Account      */ 
		
		myAccount = driver.findElement(By.linkText("My Account"));
		highLighterClick(myAccount);
		
		/*       Choisir japanese comme langue  et reptiles comme animal favori */
		
		menu1 = driver.findElement(By.name("account.languagePreference"));
		select1 = new Select(menu1);
		highLighterClick(menu1);
		select1.selectByValue("japanese");
	
		menu2 = driver.findElement(By.name("account.favouriteCategoryId"));
		select2 = new Select(menu2);
		highLighterClick(menu2);
		select2.selectByValue("REPTILES"); 
		
		
		/*        Verifier que les deux checkbox sont selectionnés     */
		
		checkbox1 = driver.findElement(By.name("account.listOption"));		
		assertTrue("le premier checkbox n'est pas selectionné ",checkbox1.isSelected());
		
		checkbox2 = driver.findElement(By.name("account.bannerOption"));		
		assertTrue("le deuxième checkbox n'est pas selectionné ",checkbox2.isSelected());
		
		
		/*  deselectionné enable My list  */
		
		highLighterClick(checkbox1);
			
	}
	
	/*   Une methode qui permet de colorer l'arrière plan d'un bouton au moment ou en click dessus    */
	
	public void highLighterClick(WebElement element){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("element.scrollIntoView();");
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",element);
		element.click();
		}

	/*   Permet de colorer et remplir un champs    */
	
	public void highLighterKeys(WebElement element, String S){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("document.getElementById('subscription-navigo-signature-nav-2').scrollIntoView();");
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",element);
		element.sendKeys(S);
		}
	
	/*-------------------------------------------------------------------*/
	
	@After    
	public void fermer() {
		driver.close();
	}   
	
	
	
}
