package TpSelenium;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TestDragDrop {
	  WebDriver driver;
	  WebElement element1;
	  WebElement element2;
	    
	@Test
	public void tester() throws InterruptedException {
		
		/*     instanciation du driver et connexion */
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://localhost/TutorialHtml5HotelPhp/");
		driver.manage().window().maximize();
		
		WebElement resa = driver.findElement(By.xpath("//div[contains(text(),'resa1')]"));
		assertTrue(resa.isDisplayed());
		
		element1 = driver.findElement(By.xpath("//div[@id='dp']/div[@style='margin-left: 240px; margin-right: 1px; position: relative;']/div[@class='scheduler_default_scrollable']/div[@class='scheduler_default_matrix']/div[@style='position: absolute;']/div[1]"));
		element2 = driver.findElement(By.xpath("//div[@class='scheduler_default_matrix']/div[@style='position: absolute;']/div[@style='left: 40px; top: 0px; width: 40px; height: 50px; position: absolute; background-color: rgb(248, 248, 248);']"));
		
		  

        Actions a = new Actions(driver);
		 Thread.sleep(7000);
		a.clickAndHold(element1).moveToElement(element2).release(element2).build().perform();
        Thread.sleep(7000);
		
			
	}

}
