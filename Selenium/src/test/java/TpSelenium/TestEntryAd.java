package TpSelenium;

import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestEntryAd {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement close;
	
		@Before
		public void ouvrir() { 
		
	          /* instanciation du driver et wait */
			
			System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			wait = new WebDriverWait(driver,20);
		}
		
		@Test
		public void naviguer() {
		
			/*  Se connecter à l'url   */
			
			driver.get("https://the-internet.herokuapp.com/entry_ad");
			driver.manage().window().maximize();
		
			 /* Gerer la fenêtre qui apparait (ou pas) */
			
			// explicit wait
		    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='modal-title']")));
		    close = driver.findElement(By.xpath("//div[@class='modal-footer']/p"));
		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='modal-footer']/p")));   
			close.click();
					
		}
	
		@After    
		public void fermer() {
			driver.close();
		}   
	
}
