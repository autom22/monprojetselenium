package TpSelenium;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestSynchronisation {
	
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement bouton;
	WebElement userName;
	WebElement login;
	WebElement password;
	WebElement boutonLogin;
	WebElement page;
	WebElement logOut;
	WebElement menu;
	WebElement element;
	
	
	@Test
	public void tester() {
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		
		
		// implicit wait
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.NANOSECONDS);
		wait = new WebDriverWait(driver,1);
		
		driver.get("https://katalon-demo-cura.herokuapp.com");
		driver.manage().window().maximize();
		
		
		// explicit wait
	    wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Make Appointment")));   
		bouton = driver.findElement(By.linkText("Make Appointment"));
		
	 /*	WebElement element = wait.until(ExpectedConditions.visibilityOfElement("bouton"); */
		
		
		
		bouton.click();
		/* login = driver.findElement(By.id("login"));  */
		
	     wait.until(ExpectedConditions.elementToBeClickable(By.id("login")));  
	/* 	assertTrue("La page de connexion ne s'affiche pas",login.isDisplayed());  */
		/*______________________________________________________________________________*/
		userName = driver.findElement(By.id("txt-username"));
		userName.clear();  userName.sendKeys("John Doe");     
		password = driver.findElement(By.id("txt-password"));
		password.clear();  password.sendKeys("ThisIsNotAPassword"); 
		boutonLogin = driver.findElement(By.id("btn-login")); 
		boutonLogin.click();
		page = driver.findElement(By.id("appointment"));
		assertTrue("La page de connexion ne s'affiche pas",page.isDisplayed());  
		/*________________________________________________________________________________*/
	
		menu = driver.findElement(By.id("menu-toggle"));
		menu.click();
		logOut = driver.findElement(By.linkText("Logout"));
		logOut.click();  
		
		
		
		
	}
	
	
	@After    
	public void fermer() {
		driver.close();
	}   

}
