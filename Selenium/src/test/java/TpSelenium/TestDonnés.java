package TpSelenium;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestDonnés {
	
	WebDriver driver;
	WebElement username;
	String userName;
	String motDePasse;
	WebElement signIn;
	WebElement logIn;
	WebElement dogs;
	WebElement cellule;
	String pfile;
	
	@Test
	public void naviguer() throws IOException {
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");	
		
		/* ********** Charger le JDD dans la class de test  ***********  */
		
		ArrayList<Map<String, String>> 	listJdd = loadCsvJDD();
		
		
		userName = listJdd.get(0).get("login");
	    motDePasse = listJdd.get(0).get("mot de passe");
	    
		signIn = driver.findElement(By.linkText("Sign In"));
		signIn.click();
		driver.manage().window().maximize();
		username = driver.findElement(By.name("username"));
		username.sendKeys(userName);
		logIn = driver.findElement(By.name("signon"));
		logIn.click();
		
		/* *******************  DOGS  ************************ */
		
		dogs = driver.findElement(By.xpath("//div[@id='SidebarContent']/a[2]"));
		dogs.click();
		
		//tableDogs = driver.findElement(By.tagName("h2"));
		//assertTrue("Le tableau n'est pas affiché",tableDogs.isDisplayed());
		
		String S= "Dalmation";
		int ligne = retournerNumeroDeLigne(S);
		System.out.println("laligne est " + ligne);
		cellule = getCellule(ligne,1);
		cellule.click();
		
		
		pfile = "src/main/java/chien.txt";
		TextFileWriting(pfile, "Dalmation");
		
		
	}
	
	public ArrayList<Map<String, String>> loadCsvJDD () throws IOException {
		String csvFilePath = "src/main/java/fileNew.csv";
		ArrayList<Map<String, String>> listJDD = new ArrayList<>();
		List<String[]> list = Files.lines(Paths.get(csvFilePath)).map(line -> line.split("\\\\r?\\\\n")).collect(Collectors.toList());
		
	    	for (int j = 1; j < list.size(); j++) {
	      	Map<String, String> jdd = new HashMap<>();
	    	String[] titres = list.get(0)[0].split(",");
	    	String[] val = list.get(j)[0].split((","));
	        	for (int i = 0; i < titres.length; i++) {
		            jdd.put(titres[i], val[i]);
		        }
		    listJDD.add(jdd);
		    }
		return listJDD;	
	}
	
	 /* retourne le numero de la ligne d'un element donné  */ 
	
	   public int retournerNumeroDeLigne(String s){ 
		   int ligneCourante = 1;
		   List<WebElement> l_lignes = driver.findElements(By.xpath("//table/tbody/tr"));
		   for(WebElement ligne : l_lignes){
		     List<WebElement> l_cell = ligne.findElements(By.xpath("td"));
		     for(WebElement cell:l_cell){
		         if(cell.getText().equals(s)){
		         return ligneCourante;
		         }
		       }
		       ligneCourante ++;
		}
		return -1;
	
	   }
	
	   public WebElement getCellule(int row, int col){ 
		   WebElement element = driver.findElement(By.xpath("//table/tbody/tr["+row+"]/td["+col+"]"));
		   return element;
		  }
	
	
	   public static void TextFileWriting(String pfile, String ptext) throws IOException {
			 try {
			FileOutputStream outputStream = new FileOutputStream(pfile,true);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
			 bufferedWriter.write(ptext);
			 bufferedWriter.close();
			 } catch (IOException e) {
			 System.out.println("un problème avec le fichier " + pfile);
			throw e;
			 }
			 }
	   
	   
}
