package TpSelenium;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestMenuDeroulant {
	WebDriver driver;
	WebElement signIn;
	WebElement userName;
	WebElement logIn;
	WebElement myAccount;
	WebElement menu1;
	Select select1;
	WebElement menu2;
	Select select2;
	WebElement checkbox1;
	WebElement checkbox2;
	
	@Test
	public void naviguer() {
		
		
		
		/* instanciation du driver */
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		
		
		/*  Se connecter à l'url   */
		
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		signIn = driver.findElement(By.linkText("Sign In"));
		signIn.click();
		driver.manage().window().maximize();
		userName = driver.findElement(By.name("username"));
		
		
         /*     Identification      */
		
		userName.sendKeys("j2ee");
		logIn = driver.findElement(By.name("signon"));
		logIn.click();
		
		/*       Se connecter à My Account      */ 
		
		myAccount = driver.findElement(By.linkText("My Account"));
		myAccount.click();
		
		/*       Choisir japanese comme langue  et reptiles comme animal favori */
		
		menu1 = driver.findElement(By.name("account.languagePreference"));
		select1 = new Select(menu1);
		select1.selectByValue("japanese");
	
		menu2 = driver.findElement(By.name("account.favouriteCategoryId"));
		select2 = new Select(menu2);
		select2.selectByValue("REPTILES");  
		
		checkbox1 = driver.findElement(By.name("account.listOption"));		
		assertTrue(checkbox1.isSelected());
		
		checkbox2 = driver.findElement(By.name("account.bannerOption"));		
		assertTrue(checkbox2.isSelected());
		
		checkbox1.click();
		
		
		
		
	}
	
	@After
	public void fermer() {
		driver.close();
	} 

}
