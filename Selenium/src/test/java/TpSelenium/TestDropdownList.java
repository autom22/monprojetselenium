package TpSelenium;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestDropdownList {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement menu;
	Select select;
	
	@Before
	public void ouvrir() { 
	
          /* instanciation du driver et implicit wait */
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver,10);
	}
	
	@Test
	public void naviguer() {
	
		/*  Se connecter à l'url   */
		
		driver.get("https://the-internet.herokuapp.com/dropdown");
		driver.manage().window().maximize();
		
		/*  choisir l'option 2 dans le menu deroulant    */ 
		
		menu = driver.findElement(By.id("dropdown"));
		highLighterClick(menu);
		select = new Select(menu);
		select.selectByValue("2");
	   		
	}
	
	public void highLighterClick(WebElement element){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",element);
		element.click();
		}
	
	/*-------------------------------------------------------------------*/
	
	@After    
	public void fermer() {
		driver.close();
	}   
	
}
