package TpSelenium;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestJpetStore {
	WebDriver driver;
	WebElement signIn;
	WebElement userName;
	WebElement logIn;
	WebElement fish;
	WebElement table;
	WebElement tigerShark;
	WebElement tooth;
	WebElement add;
	WebElement panier;
	
	
@Test
	public void naviguer() {
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		 /* signIn = new WebElement(); */ 
		signIn = driver.findElement(By.linkText("Sign In"));
		signIn.click();
		driver.manage().window().maximize();
		userName = driver.findElement(By.name("username"));
 
		userName.sendKeys("j2ee");
		logIn = driver.findElement(By.name("signon"));
		logIn.click();
		
		assertTrue("Le sign Out n'est pas affiché",driver.findElement(By.linkText("Sign Out")).isDisplayed());
		
		/*_____________________________________________________________*/
		
		
		fish = driver.findElement(By.xpath("//body/div[@id='Content']/div[@id='Main']/div[@id='Sidebar']/div[@id='SidebarContent']/a"));
		fish.click();
		table = driver.findElement(By.tagName("table"));
		assertTrue("Le tableau n'est pas affiché",table.isDisplayed());

		/*_________________________________________________________________________________________________*/
		
		
		tigerShark = driver.findElement(By.linkText("FI-SW-02"));
		tigerShark.click();
		tooth = driver.findElement(By.xpath("//table/tbody/tr[2]/td[3]"));
	    assertTrue("Le produit n'est pas affiché",tooth.isDisplayed()); 
		
		/*________________________________________________________________________________________*/
	    
	    
	    add = driver.findElement(By.linkText("Add to Cart"));
	    add.click();
	/*     panier = driver.findElement(By.linkText("Shopping Cart"));  
	    
	    
	    assertTrue("Le panier n'est pas affiché",panier.isDisplayed());  */ 
	   
	   
	   
		
	
	}
	
     @After
     public void fermer() {
	     driver.close();
     }


}
