package TpSelenium;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.collect.Table.Cell;

public class TestTableau {
     
	WebDriver driver;
	WebElement signIn;
	WebElement userName;
	WebElement logIn;
	WebElement dogs;
	WebElement tableDogs;
	WebElement cellule;

	@Test
	public void tester() {
		
		/*     instanciation du driver et connexion */
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		signIn = driver.findElement(By.linkText("Sign In"));
		signIn.click();
		driver.manage().window().maximize();
		
		userName = driver.findElement(By.name("username"));
		userName.clear();
		userName.sendKeys("j2ee");
		
		logIn = driver.findElement(By.name("signon"));
		logIn.click();
		
		assertTrue("Le sign Out n'est pas affiché",driver.findElement(By.linkText("Sign Out")).isDisplayed());
		
		/*    Selectionner la categorir DOGS    */
		
		dogs = driver.findElement(By.xpath("//body/div[@id='Content']/div[@id='Main']/div[@id='Sidebar']/div[@id='SidebarContent']/a[2]"));
		dogs.click();
		tableDogs = driver.findElement(By.tagName("h2"));
		assertTrue("Le tableau n'est pas affiché",tableDogs.isDisplayed());
		String S= "Dalmation";
		int ligne = retournerNumeroDeLigne(S);
		System.out.println("laligne est " + ligne);
		cellule = getCellule(ligne,1);
		cellule.click();
		
		
		
	}
		
	
		
	  /* retourne le numero de la ligne d'un element donné  */ 
	
	   public int retournerNumeroDeLigne(String s){ 
		   int ligneCourante = 1;
		   List<WebElement> l_lignes = driver.findElements(By.xpath("//table/tbody/tr"));
		   for(WebElement ligne : l_lignes){
		     List<WebElement> l_cell = ligne.findElements(By.xpath("td"));
		     for(WebElement cell:l_cell){
		         if(cell.getText().equals(s)){
		         return ligneCourante;
		         }
		       /*   System.out.println("J'ai fini mon boucle");  */
		       }
		       ligneCourante ++;
		     /*   System.out.println(ligneCourante);  */
		}
		 /*  System.out.println("j'ai pas trouvé " + s + " dans le tableau");*/
		return -1;
	
	   }
	
	   
	   
	   /*  retourne le webelement ligne row collonne col  */ 
	   
	   public WebElement getCellule(int row, int col){ 
		   WebElement element = driver.findElement(By.xpath("//table/tbody/tr["+row+"]/td["+col+"]"));
		   return element;
		  }

	
	   
		@After
		public void fermer() {
			driver.close();
		}
}
