package TpSelenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestSwithTo {

	
     WebDriver driver;
	 WebElement cellule1;
     WebElement popUp;
     WebElement reservation;
     WebElement save;
     WebElement cellule2;
	@Test
	public void tester() {
		
		/*     instanciation du driver et connexion */
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://localhost/TutorialHtml5HotelPhp/");
		driver.manage().window().maximize();
		
	    cellule1 = driver.findElement(By.xpath("//div[@class='main']/div[@style='margin-left: 160px;']/div[@id='dp']/div[@style='margin-left: 240px; margin-right: 1px; position: relative;']/div[@class='scheduler_default_scrollable']/div[@class='scheduler_default_matrix']/div[@style='position: absolute;']/div[1]"));
		cellule1.click();
		driver.switchTo().frame(0);
		reservation = driver.findElement(By.id("name"));
		reservation.click();
		reservation.sendKeys("resa1");
		save = driver.findElement(By.xpath("//html/body/form/div[@class='space']/input"));
		save.click();
		driver.switchTo().defaultContent();

		// cellule2 = driver.findElement(By.partialLinkText("resa1")); 
	
	   //  assertTrue(cellule2.isDisplayed());
		WebElement resa = driver.findElement(By.xpath("//div[content(text(),'resa1')]"));
		assertTrue(resa.isDisplayed());
		
	
	}
	
	@After
	public void fermer() {
		driver.close();
	}
	
	
}
