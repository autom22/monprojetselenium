package TpSelenium;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestRecapHotel {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement celluleResa;
	WebElement celluleResa2;
	WebElement celluleResa3;
    WebElement date1;
    WebElement date2;
    WebElement date3;
    WebElement date4;
    WebElement reservation;
    WebElement save;
    WebElement element1;
    WebElement element2;
    WebElement croix;
    LocalDate D;
    LocalDate D2;
    LocalDate D3;
    String S;
    String S2;
    String S3;
    Actions a;
    DateTimeFormatter formatDate;
    
    
    
	@Test
	public void tester() throws InterruptedException {
		
	 /* *************   Driver et connexion : ********    */
	System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
	driver = new ChromeDriver();
	driver.get("http://localhost/TutorialHtml5HotelPhp/");
	driver.manage().window().maximize();
	    
	 /* *************   Implicit Wait : ********    */
	
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	wait = new WebDriverWait(driver,5);
	
	/*  *************  Les locators : ***************   */
	 
      celluleResa = driver.findElement(By.xpath("//div[@id='dp']/div[@style='margin-left: 240px; margin-right: 1px; position: relative;']/div[@class='scheduler_default_scrollable']/div[@class='scheduler_default_matrix']/div[@style='position: absolute;']/div[2]"));
      
      /* reservation = driver.findElement(By.id("name"));
	 save = driver.findElement(By.xpath("//html/body/form/div[@class='space']/input"));
	*/				

	   /* *************   Reservation date du jour : ****************    */
    celluleResa.click();
	driver.switchTo().frame(0);
	
	reservation = driver.findElement(By.id("name"));
	reservation.click();
	reservation.sendKeys("resa1");
	date1 = driver.findElement(By.id("start")); 
	date1.click();
	formatDate = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	
	D = LocalDate.now();
	S = D.toString();
	date1.clear();
	date1.sendKeys(S);
	
	date2 = driver.findElement(By.id("end"));
	date2.click();
	date2.clear();
	D2 = D.plusDays(1);
	S2 = D2.toString();
	date2.sendKeys(S); 
	
	save = driver.findElement(By.xpath("//div[@class='space']/input"));
	save.click();
	driver.switchTo().defaultContent();  
	
	/* *************   Bouger au lendemain : ****************    */
	
	
	celluleResa2 = driver.findElement(By.xpath("//div[contains(text(),'resa1')]"));
	celluleResa2.click();
	driver.switchTo().frame(0);
	date3 = driver.findElement(By.id("start")); 
	date3.click();
	date3.clear();
	date3.sendKeys(S2);
	date4 = driver.findElement(By.id("end"));
	date4.click();
	date4.clear();
	D3 = D2.plusDays(1);
	S3 = D3.toString();
	date4.sendKeys(S2);
	save = driver.findElement(By.xpath("//div[@class='space']/input"));
	save.click();
	driver.switchTo().defaultContent(); 
	
	/* *************   Supprimer la reservation : ****************    */
	celluleResa3 = driver.findElement(By.xpath("//div[contains(text(),'resa1')]"));
  /*  Actions b = new Actions(driver);
	Thread.sleep(7000);
	 b.moveToElement(celluleResa3).build().perform();
	Thread.sleep(7000);  */
	
	
	//croix = driver.findElement(By.xpath("//div[@class='scheduler_default_event_delete']")); 
	croix = driver.findElement(By.className("scheduler_default_event_delete"));
	//croix.click();
	
	
	
	/* Actions a = new Actions(driver);
	a.moveToElement(element).moveToElement(driver.findElement(…)).click().build().perform();  */
	
	}

}
