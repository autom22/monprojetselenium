package TpSelenium;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TestMouseHover {
	
	WebDriver driver;
	WebElement resa;
	WebElement element;
	WebElement croix;
	
	@Test
	public void tester() throws InterruptedException {
		
		/*     instanciation du driver et connexion */
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();	
		driver.get("http://localhost/TutorialHtml5HotelPhp/");
		driver.manage().window().maximize();
		
		/*     les locators     */
	    resa = driver.findElement(By.xpath("//div[contains(text(),'resa1')]"));
        element = driver.findElement(By.xpath("//div[@id='dp']//div[@class='scheduler_default_matrix']/div[@style='position: absolute;']/div[1]"));

		/*     verifier qu'on a déjà une reservation */
		assertTrue(resa.isDisplayed());
		
		/*     Se positionner sur la cellule et annuler la reservation */
		Actions a = new Actions(driver);
		a.moveToElement(element).build().perform();
		
		 croix = driver.findElement(By.xpath("//div[@class='scheduler_default_event_delete' and @style='width: 17px; height: 17px; right: 2px; top: 5px')]"));
		// croix = driver.findElement(By.xpath("//div[@class='scheduler_default_event_delete']"));
		// croix = driver.findElement(By.className("scheduler_default_event_delete"));		//croix = driverfinfElement		
		croix.click();
		
	}
	
	
	
	
}
