package TpSelenium;
import static org.junit.Assert.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MaClassDeTest {
	
	WebDriver driver;
	
	
	@Before
	public void avant() {
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	

	@Test
	public void naviguer() {
		
		driver.get("https://latavernedutesteur.fr");
		driver.manage().window().maximize();
		
		System.out.println(driver.getTitle());
		assertEquals("Ce n'est pas le même titre", driver.getTitle(),"La taverne du testeur");
	
		
		
		/*  fail();   */
	}


	@After
	public void fermer() {
		driver.close();
	}
	
}
