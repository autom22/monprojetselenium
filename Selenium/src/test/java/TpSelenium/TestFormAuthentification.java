package TpSelenium;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestFormAuthentification {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement motDePasse;
	WebElement identifiant;
	WebElement logIn;
	

	@Before
	public void ouvrir() { 
	
          /* instanciation du driver et wait */
		
		System.setProperty("webdriver.chrome.driver","src/main/java/TpSelenium/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver,20);
	}
	
	
	@Test
	public void naviguer() {
	
		/*  Se connecter à l'url   */
		
		driver.get("https://the-internet.herokuapp.com/login");
		driver.manage().window().maximize();

		/* S'identifier avec des faux identifiants */
		
		identifiant = driver.findElement(By.id("username"));
		motDePasse = driver.findElement(By.id("password"));
		
		
		identifiant.sendKeys("admin");
		motDePasse.sendKeys("admin");
		
		logIn = driver.findElement(By.xpath("//button[@type='submit']/i"));
		logIn.click();
		// assertTrue();.....exercice incomplet
		
		
	}	
	

	@After    
	public void fermer() {
		driver.close();
	}   
	
	
}
